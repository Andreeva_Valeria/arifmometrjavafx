package sample;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField inputIntNumber1;

    @FXML
    private TextField inputIntNumber2;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClear;

    @FXML
    private TextField amount;

    @FXML
    void add(ActionEvent event) {
        if (inputIntNumber1.getText().isEmpty() || inputIntNumber2.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(inputIntNumber1);
            Shake secondOperandAnim = new Shake(inputIntNumber2);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            return;
        }
        int firstNumber = Integer.parseInt(inputIntNumber1.getText());
        int secondNumber = Integer.parseInt(inputIntNumber2.getText());
        int sum = firstNumber + secondNumber;
        amount.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        inputIntNumber1.clear();
        inputIntNumber2.clear();
        amount.clear();
    }
}
